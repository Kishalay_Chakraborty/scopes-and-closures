function counterFactory() {

    let counterVariable = 0;

    return {

        increment() {
            counterVariable += 1;
            return counterVariable;
        },

        decrement() {
            counterVariable -= 1;
            return counterVariable;
        }
    }
}

module.exports = counterFactory;