const cacheFunction = require('../cacheFunction.cjs');

const result = cacheFunction((x) => x*2);

console.log(result(2));