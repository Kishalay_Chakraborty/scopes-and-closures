const limitFunctionCallCount = require('../limitFunctionCallCount.cjs');

const result = limitFunctionCallCount(() => {return "Done"}, 3);

console.log(result());
console.log(result());
console.log(result());
console.log(result());