function limitFunctionCallCount(cb, n) {

    let callCount = 0;

    return function result() {

        if(callCount < n) {
            callCount++;
            return cb();
        }
        else {
            return null;
        }
    };
}

module.exports = limitFunctionCallCount;