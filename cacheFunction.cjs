function cacheFunction(cb) {

    const cache = {};

    return function result(parameter) {

        if(cache[parameter]) {
            return cache[parameter];
        }

        let cbFunctionCall = cb(parameter);
        cache[parameter] = cbFunctionCall;

        return cbFunctionCall;
    }
}

module.exports = cacheFunction;